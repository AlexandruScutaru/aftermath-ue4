// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
#pragma once 
#include "GameFramework/HUD.h"
#include "Inventory_CraftingHUD.generated.h"

UCLASS()
class AInventory_CraftingHUD : public AHUD
{
	GENERATED_BODY()

public:
	AInventory_CraftingHUD();

	/** Primary draw call for the HUD */
	virtual void DrawHUD() override;

private:
	/** Crosshair asset pointer */
	class UTexture2D* CrosshairTex;

};

