#pragma once

#include "Engine/DataTable.h"
#include "InventoryItem.generated.h"


#define ITEM_COUNT 14

USTRUCT(BlueprintType)
struct FCraftingInfo : public FTableRowBase{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FText Description;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FName RecipeID;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FName ComponentID;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FName ProductID;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool bDestroyComponent;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool bDestroyBase;

};

USTRUCT(BlueprintType)
struct FInventoryItem : public FTableRowBase{
	GENERATED_BODY()

public:
	FInventoryItem(){
		Name = FText::FromString("Item");
		Action = FText::FromString("Use");
		Description = FText::FromString("Please enter a description");
		Count = 0;
	}

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FName ItemID;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TSubclassOf<class APickup> ItemPickup;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FText Name;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FText Action;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 Count;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UTexture2D* Thumbnail;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FText Description;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<FCraftingInfo> CraftingCombinations;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool bCanBeUsed;

	bool operator==(const FInventoryItem& item) const {
		if(ItemID == item.ItemID)
			return true;
		else
			return false;
	}
};
