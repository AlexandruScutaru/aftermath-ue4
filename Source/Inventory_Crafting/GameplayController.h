// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/PlayerController.h"
#include "Inventory_CraftingCharacter.h"
#include "GameplayController.generated.h"

/**
 * 
 */
UCLASS()
class INVENTORY_CRAFTING_API AGameplayController : public APlayerController
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintImplementableEvent)
	void ReloadInventory();

	UFUNCTION(BlueprintCallable, Category = "Utils")
	void AddItemToInventoryByID(FName ID);

	UFUNCTION(BlueprintCallable, Category = "Utils")
	void CraftItem(FInventoryItem Component, FInventoryItem Base, AGameplayController* Controller);
	
	UFUNCTION(BlueprintCallable, Category = "Utils")
	bool CanCraftItem(FInventoryItem comp, FInventoryItem base, FName RecipeID);

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere)
	class AInteractable* CurrentInteractable;
	
	UPROPERTY(BlueprintReadWrite, VisibleAnyWhere)
	TArray<FInventoryItem> Inventory;

protected:
	void Interact();
	virtual void SetupInputComponent() override;

private:
	void DecreaseCount(FInventoryItem& item);

};
