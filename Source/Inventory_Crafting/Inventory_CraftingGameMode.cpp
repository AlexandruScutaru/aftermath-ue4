// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#include "Inventory_Crafting.h"
#include "Inventory_CraftingGameMode.h"
#include "Inventory_CraftingHUD.h"
#include "Inventory_CraftingCharacter.h"

AInventory_CraftingGameMode::AInventory_CraftingGameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = AInventory_CraftingHUD::StaticClass();
}
