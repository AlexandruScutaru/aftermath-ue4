// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Interactable.h"
#include "InventoryItem.h"
#include "Storage.generated.h"

/**
 * 
 */
UCLASS()
class INVENTORY_CRAFTING_API AStorage : public AInteractable
{
	GENERATED_BODY()
	
public:
	AStorage();

	UFUNCTION(BlueprintCallable, Category = "Storage")
	void init();

	UPROPERTY(BlueprintReadWrite, VisibleAnyWhere)
	TArray<FInventoryItem> Loot;

protected:
	UPROPERTY(EditAnywhere)
	UStaticMeshComponent* PickupMesh;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FName StorageID;
	
};
