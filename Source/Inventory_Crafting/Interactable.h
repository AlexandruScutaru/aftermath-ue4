// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "Interactable.generated.h"

UCLASS()
class INVENTORY_CRAFTING_API AInteractable : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AInteractable();

	/*
	* This function is called when the player interacts with the Interactable
	* This function is marked as BlueprintImplementable, so this can be
	* implemented using blueprints
	*/
	UFUNCTION(BlueprintImplementableEvent)
	void Interact(APlayerController* Controller);

	UPROPERTY(EditDefaultsOnly)
	FString Name;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FString Action;

	UFUNCTION(BlueprintCallable, Category = "Pickup")
	FString GetUseText()const { return FString::Printf(TEXT("%s : Press E to %s"), *Name, *Action); }

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

};
