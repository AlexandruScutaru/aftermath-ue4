// Fill out your copyright notice in the Description page of Project Settings.

#include "Inventory_Crafting.h"
#include "Storage.h"
#include "GameplayGameMode.h"


AStorage::AStorage(){
	PickupMesh = CreateDefaultSubobject<UStaticMeshComponent>("PickupMesh");
	StorageID = FName("Please enter an ID");
}

void AStorage::init(){
	AGameplayGameMode* GameMode = Cast<AGameplayGameMode>(GetWorld()->GetAuthGameMode());
	UDataTable* ItemTable = GameMode->GetItemDB();
	FInventoryItem* itemToAdd;
	int32 amount = FMath::RandRange(0, 5);
	for(int32 i = 0; i < amount; i++){
		int index = FMath::RandRange(0, 13);
		FString idStr = FString::FromInt(index);
	    itemToAdd = ItemTable->FindRow<FInventoryItem>(FName(*idStr), "");

		itemToAdd->Count = 1;
		Loot.Add(*itemToAdd);
	}
}
