// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Interactable.h"
#include "Door.generated.h"

/**
 * 
 */
UCLASS()
class INVENTORY_CRAFTING_API ADoor : public AInteractable
{
	GENERATED_BODY()
public:
	ADoor();

protected:
	UPROPERTY(EditAnywhere)
	UStaticMeshComponent* PickupMesh;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FName DoorID;
	
	
};
