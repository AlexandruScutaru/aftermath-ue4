// Fill out your copyright notice in the Description page of Project Settings.

#include "Inventory_Crafting.h"
#include "Interactable.h"
#include "GameplayGameMode.h"
#include "GameplayController.h"


void AGameplayController::AddItemToInventoryByID(FName ID){
	AGameplayGameMode* GameMode = Cast<AGameplayGameMode>(GetWorld()->GetAuthGameMode());
	UDataTable* ItemTable = GameMode->GetItemDB();

	FInventoryItem* itemToAdd = ItemTable->FindRow<FInventoryItem>(ID, "");
	if(itemToAdd){
		for(int32 i = 0; i != Inventory.Num(); ++i){
			if(Inventory[i] == *itemToAdd){
				Inventory[i].Count++;
				return;
			}
		}
		itemToAdd->Count = 1;
		Inventory.Add(*itemToAdd);
	}
}

void AGameplayController::CraftItem(FInventoryItem Component, FInventoryItem Base, AGameplayController * Controller){
	//check if we made a valid craft
	for(auto Craft : Base.CraftingCombinations){
		if(Craft.ComponentID == Component.ItemID){
			if(Craft.bDestroyComponent){
				DecreaseCount(Component);
			}
			if(Craft.bDestroyBase){
				DecreaseCount(Base);
			}

			AddItemToInventoryByID(Craft.ProductID);
			ReloadInventory();
		}
	}
}

void AGameplayController::Interact(){
	if(CurrentInteractable){
		CurrentInteractable->Interact(this);
	}
}

void AGameplayController::SetupInputComponent(){
	Super::SetupInputComponent();

	InputComponent->BindAction("Use", IE_Pressed, this, &AGameplayController::Interact);
}

void AGameplayController::DecreaseCount(FInventoryItem& item){
	for(int32 i = 0; i != Inventory.Num(); ++i){
		if(Inventory[i] == item){
			if(Inventory[i].Count > 1)
				Inventory[i].Count--;
			else
				Inventory.RemoveSingle(Inventory[i]);
			return;
		}
	}
}

bool AGameplayController::CanCraftItem(FInventoryItem comp, FInventoryItem base, FName RecipeID){
	if(Inventory.Num() != 20){
		return true;
	} else{
		int craftIndex;
		for(int32 i = 0; i != base.CraftingCombinations.Num(); ++i){
			if(base.CraftingCombinations[i].RecipeID == RecipeID){
				craftIndex = i;
				break;
			}
		}
		if(base.CraftingCombinations[craftIndex].bDestroyBase && base.Count == 1)
			return true;
		if(base.CraftingCombinations[craftIndex].bDestroyComponent && comp.Count == 1)
			return true;
	}
	
	return false;
}


