// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "GameFramework/GameModeBase.h"
#include "Inventory_CraftingGameMode.generated.h"

UCLASS(minimalapi)
class AInventory_CraftingGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AInventory_CraftingGameMode();
};



